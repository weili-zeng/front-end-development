// 引入 vue
import Vue from 'vue'
// 引入App
import App from './App.vue'

import {hunhe} from './mixin'

// 关闭Vue的生产提示
Vue.config.productionTip = false

Vue.mixin(hunhe)  // 所有的vc和vm 都会得到 hunhe

//创建vm
new Vue({
    el: '#app',
    render: h => h(App)
})